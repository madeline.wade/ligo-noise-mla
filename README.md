# ligo-noise-mla

## Description
Collection of machine learning algorithms used for the identification of transient noise events in LIGO strain data.  The input to each MLA is a reduced set of features from a subset of LIGO auxiliary channels..

## GIANTS

Glitch Identification Algorithm using Neural neTworkS (GIANTS) is a neural network that takes all of the features from all safe auxiliary channels during a specific time window as input and makes one prediction about the probability of the presence of a glitch in LIGO data during that time window.

## TITAN

Tuned Identifier of Transient Auxiliary Noise (TITAN) is a hierarchical MLA built from neural networks.  There are 12 neural networks that are each trained on reduced features from auxilary data within a specific LIGO subsystem.  Each neural network makes a prediction about the presence of a glitch in LIGO strain data based on its inputs.  All 12 predictions are then combined by a "combiner", which is another neural network, for one final predicition about the presence of a glitch in LIGO data during the given time window.

## GOLIATH

?? (GOLIATH) is also a hierarchical MLA built from neural networks, which was built as an evolution to TITAN.  Instead of each neural network being trained on data from a specific subsystem, in GOLIATH each neural network is trained on specific glitch types as defined in LIGO's O3 noise paper (add link).  There is also one neural network that is a "catch all" for all glitches not specifically trained elsewhere.  There is a combiner neural network that makes the final prediction about the presence of glitches in LIGO data during the given time window.

## Installation

(Add instructions for setting up appropriate environment here)

## Authors and acknowledgment
Contributing authors to this work:
Maddie Wade
Les Wade
Donald Moffa
Kyle Rose
Paul Neubauer
Phillip Diamond
Sophie Schmitz
Kalista Wayt
Michael Van Keuren
Luke Wilson
David Chintala
Josh Temple
