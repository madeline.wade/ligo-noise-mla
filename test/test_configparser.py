from ligonoisemla.utils.config_parser import TITANConfigParser, HPTuningConfigParser
import unittest

class ConfigParserTest(unittest.TestCase):
    def test_titanconfigparser(self):
        titan_configs = TITANConfigParser("./test/titan_config.ini")

        assert titan_configs.testing_data_frame == "testing_data_frame.csv"
        assert titan_configs.training_data_frame == "training_data_frame.csv"
        assert titan_configs.total_data_frame == "all_data.csv"
        assert titan_configs.hyperparameter_tuning == False
        assert titan_configs.training == True
        assert titan_configs.testing == True
        assert titan_configs.evaluate == False
        assert titan_configs.train1_data_percent == 0.6
        assert titan_configs.train2_data_percent == 0.3
        assert titan_configs.sigma == 3
        assert titan_configs.subsystems == ["ISI", "LSC", "HPI"]
        assert titan_configs.mla_configs["ISIMLAParams"]["Layers"] == 2
        assert titan_configs.mla_configs["ISIMLAParams"]["Nodes"] == [32]
        assert titan_configs.mla_configs["ISIMLAParams"]["Activation"] == ['elu']
        assert titan_configs.mla_configs["ISIMLAParams"]["Dropout"] == [0.0, 0.0, 0.0]
        assert titan_configs.mla_configs["ISIMLAParams"]["L1_Weight"] == [0.0, 0.0]
        assert titan_configs.mla_configs["ISIMLAParams"]["L2_Weight"] == [0.0, 0.0]
        assert titan_configs.mla_configs["ISIMLAParams"]["Epochs"] == 6000
        assert titan_configs.mla_configs["ISIMLAParams"]["BatchSize"] == 16
        assert titan_configs.mla_configs["ISIMLAParams"]["Optimizer"] == "Nadam"
        assert titan_configs.mla_configs["ISIMLAParams"]["LearningRate"] == 0.001
        assert titan_configs.mla_configs["ISIMLAParams"]["Loss"] == "binary_crossentropy"
        assert titan_configs.mla_configs["ISIMLAParams"]["MetricsCompiler"] == "accuracy"
        assert titan_configs.mla_configs["ISIMLAParams"]["Monitor"] == "val_loss"
        assert titan_configs.mla_configs["ISIMLAParams"]["MinDelta"] == 0.0001
        assert titan_configs.mla_configs["ISIMLAParams"]["Patience"] == 15
        assert titan_configs.mla_configs["ISIMLAParams"]["EarlyStoppingMode"] == "auto"
        assert titan_configs.mla_configs["HPIMLAParams"]["Layers"] == 2
        assert titan_configs.mla_configs["HPIMLAParams"]["Nodes"] == [32]
        assert titan_configs.mla_configs["HPIMLAParams"]["Activation"] == ['elu']
        assert titan_configs.mla_configs["HPIMLAParams"]["Dropout"] == [0.0, 0.0, 0.0]
        assert titan_configs.mla_configs["HPIMLAParams"]["L1_Weight"] == [0.0, 0.0]
        assert titan_configs.mla_configs["HPIMLAParams"]["L2_Weight"] == [0.0, 0.0]
        assert titan_configs.mla_configs["HPIMLAParams"]["Epochs"] == 6000
        assert titan_configs.mla_configs["HPIMLAParams"]["BatchSize"] == 16
        assert titan_configs.mla_configs["HPIMLAParams"]["Optimizer"] == "Nadam"
        assert titan_configs.mla_configs["HPIMLAParams"]["LearningRate"] == 0.0001
        assert titan_configs.mla_configs["HPIMLAParams"]["Loss"] == "binary_crossentropy"
        assert titan_configs.mla_configs["HPIMLAParams"]["MetricsCompiler"] == "accuracy"
        assert titan_configs.mla_configs["HPIMLAParams"]["Monitor"] == "val_loss"
        assert titan_configs.mla_configs["HPIMLAParams"]["MinDelta"] == 0.0001
        assert titan_configs.mla_configs["HPIMLAParams"]["Patience"] == 15
        assert titan_configs.mla_configs["HPIMLAParams"]["EarlyStoppingMode"] == "auto"
        assert titan_configs.mla_configs["LSCMLAParams"]["Layers"] == 2
        assert titan_configs.mla_configs["LSCMLAParams"]["Nodes"] == [32]
        assert titan_configs.mla_configs["LSCMLAParams"]["Activation"] == ['elu']
        assert titan_configs.mla_configs["LSCMLAParams"]["Dropout"] == [0.0, 0.0, 0.0]
        assert titan_configs.mla_configs["LSCMLAParams"]["L1_Weight"] == [0.0, 0.0]
        assert titan_configs.mla_configs["LSCMLAParams"]["L2_Weight"] == [0.0, 0.0]
        assert titan_configs.mla_configs["LSCMLAParams"]["Epochs"] == 6000
        assert titan_configs.mla_configs["LSCMLAParams"]["BatchSize"] == 16
        assert titan_configs.mla_configs["LSCMLAParams"]["Optimizer"] == "Nadam"
        assert titan_configs.mla_configs["LSCMLAParams"]["LearningRate"] == 0.001
        assert titan_configs.mla_configs["LSCMLAParams"]["Loss"] == "binary_crossentropy"
        assert titan_configs.mla_configs["LSCMLAParams"]["MetricsCompiler"] == "accuracy"
        assert titan_configs.mla_configs["LSCMLAParams"]["Monitor"] == "val_loss"
        assert titan_configs.mla_configs["LSCMLAParams"]["MinDelta"] == 0.0001
        assert titan_configs.mla_configs["LSCMLAParams"]["Patience"] == 15
        assert titan_configs.mla_configs["LSCMLAParams"]["EarlyStoppingMode"] == "auto"
        assert titan_configs.model_save_path == "./savedmodel"
        assert titan_configs.model_save_name == "test_model.h5"
        assert titan_configs.acc_over_epochs_save == "acc_over_epochs.png"
        assert titan_configs.val_loss_plot_save == "plot_val_loss.png"
        assert titan_configs.log_roc_save == "log_roc.png"
        assert titan_configs.linear_roc_save == "linear_roc.png"
        assert titan_configs.histogram_save == "histogram.png"
        assert titan_configs.combined_roc_save == "combined_roc.png"
        assert titan_configs.combiner_configs["Layers"] == 2
        assert titan_configs.combiner_configs["Nodes"] == [9, 10]
        assert titan_configs.combiner_configs["Activation"] == ['relu', 'softmax']
        assert titan_configs.combiner_configs["Dropout"] == [0.0, 0.0]
        assert titan_configs.combiner_configs["L1_Weight"] == [0.0, 0.0]
        assert titan_configs.combiner_configs["L2_Weight"] == [0.0, 0.0]
        assert titan_configs.combiner_configs["Epochs"] == 6000
        assert titan_configs.combiner_configs["BatchSize"] == 16
        assert titan_configs.combiner_configs["Optimizer"] == "Nadam"
        assert titan_configs.combiner_configs["LearningRate"] == 0.001
        assert titan_configs.combiner_configs["Loss"] == "binary_crossentropy"
        assert titan_configs.combiner_configs["MetricsCompiler"] == "accuracy"
        assert titan_configs.combiner_configs["Monitor"] == "val_loss"
        assert titan_configs.combiner_configs["MinDelta"] == 0.0001
        assert titan_configs.combiner_configs["Patience"] == 15
        assert titan_configs.combiner_configs["EarlyStoppingMode"] == "auto"

    # FIXME: Add hP tuning config test

if __name__=='__main__':
    unittest.main(argv=['first-arg-isirgnored'], exit=False)
