from ligonoisemla.utils import channel_sorter
import unittest
import pandas as pd
from pandas.testing import assert_frame_equal

class ChannelSorterTest(unittest.TestCase):
    def test_sorter(self):
        df=pd.read_csv("./test/unittest_subsystem_split_data.csv")
        df = df.drop(df.columns[0], axis=1)
        dfs, subsystems=channel_sorter.channel_sorter(df)

        LSC = pd.DataFrame(data = {'H1:LSC_MICH_OUT_DQ-tstart': [3.0, 2.0, 9.0, 9.0, 4.0, 9.0, 8.0, 1.0, 4.0, 6.0], 'H1:LSC_POP_A_RF9_Q_ERR_DQ-phase': [5.0, 2.0, 3.0, 8.0, 0.0, 5.0, 9.0, 7.0, 1.0, 0.0], 'H1:LSC_REFL_A_RF45_I_ERR_DQ-amplitude': [3.0, 2.0, 4.0, 1.0, 8.0, 10.0, 8.0, 5.0, 9.0, 4.0], 'Labels': [0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0], 'GPS_Times': [7.0, 9.0, 17.0, 18.0, 5.0, 19.0, 3.0, 9.0, 12.0, 1.0]})

        ISI = pd.DataFrame(data = {'H1:ISI_GND_STS_ITMY_Y_DQ-fstart': [7.0, 9.0, 1.0, 6.0, 0.0, 5.0, 4.0, 8.0, 6.0, 2.0], 'H1:ISI_BS_ST1_BLND_Y_T240_CUR_IN1_DQ-tstart': [3.0, 8.0, 7.0, 4.0, 0.0, 1.0, 10.0, 10.0, 2.0, 7.0], 'Labels': [0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0], 'GPS_Times': [7.0, 9.0, 17.0, 18.0, 5.0, 19.0, 3.0, 9.0, 12.0, 1.0]})

        SUS = pd.DataFrame(data = {'H1:SUS_MC1_M1_DAMP_R_IN1_DQ-phase': [5.0, 7.0, 9.0, 7.0, 0.0, 5.0, 4.0, 1.0, 10.0, 7.0], 'H1:SUS_TMSY_M1_DAMP_Y_IN1_DQ-fend': [8.0, 7.0, 2.0, 0.0, 5.0, 0.0, 9.0, 3.0, 3.0, 10.0], 'H1:SUS_ITMX_M0_DAMP_P_IN1_DQ-fend': [8.0, 2.0, 10.0, 7.0, 0.0, 7.0, 1.0, 7.0, 5.0, 1.0], 'H1:SUS_PR2_M1_DAMP_Y_IN1_DQ-fend': [6.0, 0.0, 2.0, 0.0, 5.0, 3.0, 3.0, 8.0, 6.0, 5.0],'Labels': [0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0], 'GPS_Times': [7.0, 9.0, 17.0, 18.0, 5.0, 19.0, 3.0, 9.0, 12.0, 1.0]})

        ASC = pd.DataFrame(data = {'H1:ASC_AS_A_DC_YAW_OUT_DQ-snr': [5.0, 0.0, 5.0, 8.0, 5.0, 0.0, 5.0, 3.0, 4.0, 2.0], 'H1:ASC_INP1_P_OUT_DQ-fend': [9.0, 7.0, 10.0, 1.0, 5.0, 8.0, 6.0, 10.0, 7.0, 5.0], 'H1:ASC_DHARD_P_OUT_DQ-tend': [8.0, 5.0, 7.0, 2.0, 5.0, 5.0, 3.0, 1.0, 4.0, 10.0], 'H1:ASC_AS_B_DC_NSUM_OUT_DQ-fend': [0.0, 2.0, 7.0, 6.0, 7.0, 5.0, 0.0, 3.0, 7.0, 0.0],'H1:ASC_REFL_B_RF45_I_YAW_OUT_DQ-fstart': [8.0, 4.0, 3.0, 7.0, 4.0, 8.0, 8.0, 8.0, 2.0, 1.0], 'Labels': [0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0], 'GPS_Times': [7.0, 9.0, 17.0, 18.0, 5.0, 19.0, 3.0, 9.0, 12.0, 1.0]})

        assert_frame_equal(LSC, dfs["LSC"])
        assert_frame_equal(ISI, dfs["ISI"])
        assert_frame_equal(SUS, dfs["SUS"])
        assert_frame_equal(ASC, dfs["ASC"])
        assert subsystems == ["LSC", "ISI", "SUS", "ASC"]

if __name__=='__main__':
    unittest.main(argv=['first-arg-isirgnored'], exit=False)
