import json
from configparser import ConfigParser

class TITANConfigParser():
    def __init__(self, config):
        self.config = ConfigParser()
        self.config.read(config)
        self.data_upload()
        self.mode_upload()
        self.subsystem_data_upload()
        self.output_upload()
        self.combiner_upload()
        
    def data_upload(self):
        self.testing_data_frame = self.config['Data']['TestingDataFrame']
        self.training_data_frame = self.config['Data']['TrainingDataFrame']
        self.total_data_frame = self.config['Data']['TotalDataFrame']

    def mode_upload(self):
        self.hyperparameter_tuning = self.config.getboolean('Mode','Hyperparametertuning')
        self.training = self.config.getboolean('Mode','Training')
        self.testing = self.config.getboolean('Mode','Testing')
        self.evaluate = self.config.getboolean('Mode','EvaluateOnly')
        self.train1_data_percent = eval(self.config['Mode']['Train1DataPercentage'])
        self.train2_data_percent = eval(self.config['Mode']['Train2DataPercentage'])
        self.sigma = eval(self.config['Mode']['Sigma'])
        
    def subsystem_data_upload(self):
        self.mla_configs = {}
        self.subsystems = json.loads(self.config['SubSystems']['Subsystems'])
        
        for headings in self.subsystems:
            self.mla_configs[headings+'MLAParams'] = {}
        for params in self.mla_configs:
            self.layers = int(self.config[params]['Layers'])
            self.nodes = eval(self.config[params]['Nodes'])
            self.activation = eval(self.config[params]['Activation'])
            self.dropout = eval(self.config[params]['DropOut'])
            self.L1_weight = eval(self.config[params]['L1Weight'])
            self.L2_weight = eval(self.config[params]['L2Weight'])
            self.epochs = int(self.config[params]['Epochs'])
            self.batch_size = int(self.config[params]['BatchSize'])
            self.optimizer = self.config[params]['Optimizer']
            self.learning_rate = float(self.config[params]['LearningRate'])
            self.loss = self.config[params]['Loss']
            self.metrics_compiler = self.config[params]['MetricsCompiler']
            self.monitor = self.config[params]['Monitor']
            self.min_delta = float(self.config[params]['MinDelta'])
            self.patience = int(self.config[params]['Patience'])
            self.early_stopping_mode = self.config[params]['EarlyStoppingMode']
            
            self.mla_configs[params] = {'Layers': self.layers, 
                                        'Nodes': self.nodes,
                                        'Activation': self.activation,
                                        'Dropout': self.dropout,
                                        'L1_Weight': self.L1_weight,
                                        'L2_Weight': self.L2_weight,
                                        'Epochs': self.epochs,
                                        'BatchSize': self.batch_size,
                                        'Optimizer': self.optimizer,
                                        'LearningRate': self.learning_rate,
                                        'Loss': self.loss,
                                        'MetricsCompiler': self.metrics_compiler,
                                        'Monitor': self.monitor,
                                        'MinDelta': self.min_delta,
                                        'Patience': self.patience,
                                        'EarlyStoppingMode': self.early_stopping_mode}  

    def output_upload(self):
        self.model_save_path = self.config['Output']['ModelSavePath']
        self.model_save_name = self.config['Output']['ModelSaveName']        
        self.acc_over_epochs_save = self.config['Output']['AccOverEpochsFileName']
        self.val_loss_plot_save = self.config['Output']['ValLossPlotFileName']
        self.log_roc_save = self.config['Output']['LogROCFileName']
        self.linear_roc_save = self.config['Output']['LinearROCFileName']
        self.histogram_save = self.config['Output']['HistogramFileName']
        self.combined_roc_save = self.config['Output']['CombinedROCFileName']
    
    def combiner_upload(self):
        self.combiner_configs = {}
        self.layers = int(self.config['Combiner']['Layers'])
        self.nodes = eval(self.config['Combiner']['Nodes'])
        self.activation = eval(self.config['Combiner']['Activation'])
        self.dropout = eval(self.config['Combiner']['DropOut'])
        self.L1_weight = eval(self.config['Combiner']['L1Weight'])
        self.L2_weight = eval(self.config['Combiner']['L2Weight'])
        self.epochs = int(self.config['Combiner']['Epochs'])
        self.batch_size = int(self.config['Combiner']['BatchSize'])
        self.optimizer = self.config['Combiner']['Optimizer']
        self.learning_rate = float(self.config['Combiner']['LearningRate'])
        self.loss = self.config['Combiner']['Loss']
        self.metrics_compiler = self.config['Combiner']['MetricsCompiler']
        self.monitor = self.config['Combiner']['Monitor']
        self.min_delta = float(self.config['Combiner']['MinDelta'])
        self.patience = int(self.config['Combiner']['Patience'])
        self.early_stopping_mode = self.config['Combiner']['EarlyStoppingMode']
            
        self.combiner_configs = {'Layers': self.layers,
                                 'Nodes': self.nodes,
                                 'Activation': self.activation,
                                 'Dropout': self.dropout,
                                 'L1_Weight': self.L1_weight,
                                 'L2_Weight': self.L2_weight,
                                 'Epochs': self.epochs,
                                 'BatchSize': self.batch_size,
                                 'Optimizer': self.optimizer,
                                 'LearningRate': self.learning_rate,
                                 'Loss': self.loss,
                                 'MetricsCompiler': self.metrics_compiler,
                                 'Monitor': self.monitor,
                                 'MinDelta': self.min_delta,
                                 'Patience': self.patience,
                                 'EarlyStoppingMode': self.early_stopping_mode}

class HPTuningConfigParser():
    def __init__(self, config):
        self.config = ConfigParser()
        self.config.read(config)

    def data_upload(self):
        self.data_file = self.config['Data']['DataFile']

    def model_upload(self):
        self.max_trials = int(self.config['Model']['MaxTrials'])
        self.executions_per_trial = int(self.config['Model']['ExecutionsPerTrial'])
        self.kt_objective = self.config['Model']['KTObjective']
        self.kt_direction = self.config['Model']['KTDirection']
        self.early_stopping_objective = self.config['Model']['EarlyStoppingObjective']
        self.patience = int(self.config['Model']['Patience'])
        self.epochs = int(self.config['Model']['Epochs'])

    def hyperparams_upload(self):
        self.batch_size_min = int(self.config['HyperParams']['BatchSizeMin'])
        self.batch_size_max = int(self.config['HyperParams']['BatchSizeMax'])
        self.batch_size_steps = int(self.config['HyperParams']['BatchSizeSteps'])
        self.nodes_min = int(self.config['HyperParams']['NodesMin'])
        self.nodes_max = int(self.config['HyperParams']['NodesMax'])
        self.nodes_step = int(self.config['HyperParams']['NodesStep'])
        self.dropout_min = float(self.config['HyperParams']['DropoutMin'])
        self.dropout_max = float(self.config['HyperParams']['DropoutMax'])
        self.dropout_step = float(self.config['HyperParams']['DropoutStep'])
        self.activation_choices = eval(str(self.config['HyperParams']['ActivationChoices']))
        self.optimizer_choices = eval(str(self.config['HyperParams']['OptimizerChoices']))
        self.lr_choices = eval(str(self.config['HyperParams']['LRChoices']))
        self.regularizer_weight_choices = eval(str(self.config['HyperParams']['RegularizerWeightChoices']))
        self.regularizer_choices = eval(str(self.config['HyperParams']['RegularizerChoices']))
