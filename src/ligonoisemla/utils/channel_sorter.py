import pandas as pd

def channel_sorter(df, time_column = 'GPS_Times', labels_column = 'Labels'):
    df_features = df.drop([time_column, labels_column], axis=1)
    columns_by_subsystem = {}
    # Group columns by their prefix
    for col in df_features.columns:
        # The subsystem should be a 3 letter key
        # that follows [ifo]:, such as 'H1:PSL...'
        subsystem = col[3:6]
        if subsystem not in columns_by_subsystem:
            columns_by_subsystem[subsystem] = []
        columns_by_subsystem[subsystem].append(col)

    # Dictionary of dataframes keyed by subsystem
    dfs_by_subsystem = {}
    
    # Create each subsystem dataframe and
    # add to appropriate dictionary key
    for subsystem, cols in columns_by_subsystem.items():
        dfs_by_subsystem[subsystem] = pd.concat([df_features[cols], df[labels_column], df[time_column]], axis=1)
    subsystems = [key for key in dfs_by_subsystem]
    return dfs_by_subsystem, subsystems
